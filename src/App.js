import React, { useEffect, useState } from "react";
import "./App.css";
import { CallApiGet } from "./components/Api";
import CountryDetail from "./components/CountryDetail";
import ModalCountry from "./components/ModalCountry";
import ImageSlider from "./components/ImageSlider";

function App() {
  const [data, setData] = useState([]);
  const [time, setTime] = useState("");
  const fetAPI = async () => {
    const response = await CallApiGet("http://localhost:5000/country/get");
    setData(response.dataList);
    setTime(response.time);
  };

  useEffect(() => {
    fetAPI();
  }, []);

  return (
      <div style={{ position: "relative", backgroundImage: "url(/Image/background.png)" }} >
      <div style={{ position: "absolute", top: 0, right: 0, zIndex: 1, color: "white" }}>{time}</div> 
      <CountryDetail prop={data}></CountryDetail>
      <ModalCountry prop={data}></ModalCountry>
      <ImageSlider prop={data}></ImageSlider>
    </div>
  );
}

export default App;
