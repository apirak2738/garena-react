import axios from "axios";

export const CallApiGet = async (endpoint, response) => {
   try {
      const res = await axios.get(endpoint);
      response = res.data
      return response
    } catch (error) {
      console.error("Error fetching data:", error.response);
    }
  };
