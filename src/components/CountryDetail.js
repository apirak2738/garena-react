import React, { useState } from "react";
import Box from "@mui/system/Box";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import ModalCountry from "./ModalCountry";

function CountryDetail(prop) {

  
 
  const [open, setOpen] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState(null);

  const handleOpen = (country) => {
    setSelectedCountry(country);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedCountry(null);
    setOpen(false);
  };

  return (
    <dev>
      <Box className="colored-box" component="section" sx={{ p: 18, marginBottom: 0 }}>
        <div
          style={{
            backgroundColor: "#4f4b4b",
            border: "2px solid #ccc",
            padding: "10px",
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "center",
            }}
          >
            {prop.prop.map((val, index) => (
              <div
                key={val.Name}
                style={{
                  color: "#ffff",
                  margin: "10px",
                  flexBasis: "20%",
                  textAlign: "center",
                }}
              >
                <img
                  src={val.Flag}
                  alt={val.Name}
                  style={{
                    width: "100px",
                    height: "auto",
                  }}
                  onClick={() => handleOpen(val)}
                />
                <p>{val.Name}</p>
                {(index + 1) % 5 === 0 && (
                  <div style={{ width: "50%", marginBottom: "20px" }}></div>
                )}
              </div>
            ))}
          </div>
        </div>
      </Box>

      <Dialog open={open} onClose={handleClose}>
        <IconButton
          edge="end"
          color="inherit"
          onClick={handleClose}
          sx={{ position: "absolute", top: 0, right: 0 }}
        >
          <CloseIcon />
        </IconButton>
        <ModalCountry prop={selectedCountry} />
      </Dialog>
    </dev>
  );
}

export default CountryDetail