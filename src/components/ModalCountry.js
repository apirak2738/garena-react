import React from "react";
import Box from "@mui/system/Box";

const ModalCountry = (props) => {
  const { prop } = props;

  if (!prop || !prop.Flag) {
    return <div></div>;
  }

  return (
    <div style={{ overflow: "hidden" }}>
      <Box
        sx={{
          p: 2, 
          display: "flex",
          flexDirection: "column",
          alignItems: "flex-start", 
          textAlign: "left", 
          height: "100vh", 
          overflow: "hidden", 
        }}
      >
        <img
          src={prop.Flag}
          alt="Country Flag"
          style={{
            maxWidth: "100%", 
            maxHeight: "100%", 
            border: "2px solid #ccc", 
          }}
        />
        <p style={{ margin: "4px 0" }}>ชื่อประเทศ : {prop.Name}</p>
        <p style={{ margin: "2px 0" }}>อักษรย่อ : {prop.Code}</p>
        <p style={{ margin: "2px 0" }}>สกุลเงิน : {prop.CurrenciesName}</p>
        <p style={{ margin: "2px 0" }}>ภูมิภาค : {prop.Region}</p>
        <p style={{ margin: "2px 0" }}>ประชากร : {prop.Population}</p>
        <p style={{ margin: "2px 0" }}>คิดเป็นเปอร์เซ็นต์ : {prop.Percent.toFixed(2)}%</p>
        <p style={{ margin: "2px 0" }}>โซนเวลา : {prop.TimeZones}</p>
      </Box>
    </div>
  );
};

export default ModalCountry;
