import React, { useState } from "react";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Box from "@mui/system/Box";
import Dialog from "@mui/material/Dialog";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import ModalCountry from "./ModalCountry";

const ImageSlider = (prop) => {
  const [open, setOpen] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState(null);

  const handleOpen = (country) => {
    setSelectedCountry(country);
    setOpen(true);
  };

  const handleClose = () => {
    setSelectedCountry(null);
    setOpen(false);
  };
  return (
    <div>
      <Box className="colored-box" component="section" sx={{ p: 18, marginBottom: 0 }}>
        <div
          style={{
            backgroundColor: "#4f4b4b",
            border: "2px solid #ccc",
            padding: "10px",
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
        
          <Carousel>
            {prop.prop.map((val, index) => (
              <Carousel.Item key={index}>
                <img
                  className="colored-container"
                  src={val.Flag}
                  alt={`Slide ${index + 1}`}
                  style={{
                    width: "50%", 
                    height: "auto", 
                    display: "block",
                    margin: "0 auto",
                  }}
                  onClick={() => handleOpen(val)}
                />
                <div className="custom-indicator">
                  <h3>{val.caption}</h3>
                  <p>{val.description}</p>
                </div>
              </Carousel.Item>
            ))}
          </Carousel>
        </div>
      </Box>

      <Dialog open={open} onClose={handleClose}>
        <IconButton
          edge="end"
          color="inherit"
          onClick={handleClose}
          sx={{ position: "absolute", top: 0, right: 0 }}
        >
          <CloseIcon />
        </IconButton>
        <ModalCountry prop={selectedCountry} />
      </Dialog>
    </div>
  );
};

export default ImageSlider;
